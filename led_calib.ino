#include "MPU6500.h"
#include "MPU6500RM.h"
#include <Streaming.h>
#include <Wire.h>

const int WINDOW_SIZE = 100;
const float STD_THRESHOLD = 0.01;

double values[6];
double x_window[WINDOW_SIZE];
double y_window[WINDOW_SIZE];
double z_window[WINDOW_SIZE];
int cnt = 0;

double x_mean, x_std_dev;
double y_mean, y_std_dev;
double z_mean, z_std_dev;

constexpr int led = 9;
constexpr int duty_cycle = 25;

int led_value;

MPU6500 *mpu;

double mean(double *x, int size) {
  
  double sample_mean = 0;

  for (int i = 0; i < size; ++i)
    sample_mean += x[i];

  sample_mean /= size;

  return sample_mean;
}

double sum_of_squares(double *x, int size) {
  
  double sample_squares = 0;
  double sample_sum_squared = 0;
  
  for (int i = 0; i < size; ++i) {
    sample_squares += x[i] * x[i];
    sample_sum_squared += x[i];
  }
  
  sample_sum_squared *= sample_sum_squared;
  sample_sum_squared /= size;

  return sample_squares - sample_sum_squared;
}

double std_dev(double *x, int size) {
  return sqrt(sum_of_squares(x, size) / size);
}

void setup() {

  Serial.begin(115200);

  pinMode(led, OUTPUT);

  led_value = map(duty_cycle, 0, 100, 0, 255);

  mpu = new MPU6500();

  mpu->setup(MPU6500::I2C, NULL);

}

void loop() {

  mpu->sample(values);

  x_window[cnt] = values[0] / ACCEL_SF;
  y_window[cnt] = values[1] / ACCEL_SF;
  z_window[cnt] = values[2] / ACCEL_SF;

  cnt = (cnt + 1) % WINDOW_SIZE;

  // make a simple state machine where the current mean is saved when not moving
  x_mean = mean(x_window, WINDOW_SIZE);
  y_mean = mean(y_window, WINDOW_SIZE);
  z_mean = mean(z_window, WINDOW_SIZE);

  x_std_dev = std_dev(x_window, WINDOW_SIZE);
  y_std_dev = std_dev(y_window, WINDOW_SIZE);
  z_std_dev = std_dev(z_window, WINDOW_SIZE);

//  Serial << _FLOAT(abs(x_mean), 6) << " "
//         << _FLOAT(abs(y_mean), 6) << " "
//         << _FLOAT(abs(z_mean), 6) << endl;

  if (
//    (abs(x_mean) > 0.005) || (abs(y_mean) > 0.005) || (abs(z_mean) > 0.005) ||
    (abs(x_std_dev) > STD_THRESHOLD) || (abs(y_std_dev) > STD_THRESHOLD) || (abs(z_std_dev) > STD_THRESHOLD)
    )
    analogWrite(led, 0);
  else
    analogWrite(led, led_value);

    analogWrite(led, led_value);

}
